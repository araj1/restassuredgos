package ConstantUtility;

public class ConstantUrls {

    //this class consists of the required urls//

    public static final String IdentityUrl="http://35.230.156.145";
    public static final String AuthenticationUrl="http://35.230.156.145/api/v1/Identity/AuthenticateUser";


    /* credentials */
    public static final String UserName = "GosAdmin@triad.co.uk";
    public static final String Password="P@ssw0rd";

    public static String getUserName() {
        return ConstantUrls.UserName;
    }

    public static String getPassword() {
        return ConstantUrls.Password;
    }





}
