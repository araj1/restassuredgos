package ConstantUtility;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import static io.restassured.RestAssured.given;

/* this class will help to fetch the bearer token */
public class AuthenticateUser {

    public static RequestSpecification request;
    public static Response response;
    public static ValidatableResponse json;
    public static String token ;


    public void Authenticate()

    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userName", ConstantUrls.UserName);
        jsonObject.put("password", ConstantUrls.Password);


        response = given().header("Content-Type", "application/json").header("Accept", "application/json")
                .body(jsonObject.toString()).post(ConstantUrls.AuthenticationUrl).then().extract().response();

        ResponseBody body =null;
        if(response.statusCode() == 200){
            body = response.getBody();

        }

        else{

            System.out.println("Failed to get response : "+response.getStatusCode());

            }

        /* Extract the token from the response body */
        JsonPath jsonPathEvaluator = response.jsonPath();
        token = jsonPathEvaluator.get("token");


    }


    }



