package Steps;

import ConstantUtility.AuthenticateUser;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.path.json.JsonPath;
import org.json.JSONObject;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;



public class CommonStepDefs extends AuthenticateUser
{
      public static int StatusCode;

      AuthenticateUser authUser = new AuthenticateUser();

      @Test
      @Given("^I perform GET operation for \"([^\"]*)\"$")
      public void iPerformGetOperationFor(String url) throws Throwable
       {
           StatusCode = given().auth().oauth2(AuthenticateUser.token)
                        .get(url)
                        .then()
                        .extract().statusCode();

       }

    @Test
       @Given("^I perform GET operation without Authentication for \"([^\"]*)\"$")
        public void iPerformGetOprationWithoutAuthentication(String url) throws Throwable
       {
            StatusCode = when()
                        .get(url)
                        .then()
                        .extract()
                        .statusCode();

       }

    @Test
        @Then("^I should see the status code \"([^\"]*)\"$")
       public void IPerformThenOperation(int expStatus) throws Throwable
        {

            /*validates the expected status coe */
            Assert.assertEquals(expStatus,StatusCode);


        }

    @Test
       @Given("^I Authenticate the url$")
       public void Authenticate()

       {
           authUser.Authenticate();

       }

    @Test
       @Given("^I perform Post action \"([^\"]*)\" for the emailId \"([^\"]*)\"$")
       public void IPerformPutAction(String URL, String EmailId)
       {
        JSONObject request = new JSONObject();
        request.put("emailAddress",EmailId);

        response=  given()
                  .auth().oauth2(AuthenticateUser.token)
                  .header("Accept", "application/json")
                  .header("Content-Type", "application/json")
                  .body(request.toString())
                  .post(URL);

        StatusCode = response.statusCode();

        JsonPath jsonPathEvaluator = response.jsonPath();
        String email = jsonPathEvaluator.get("message");

        /* to fetch only emailId from the response */
        String [] data = email.split(" ");
        Assert.assertEquals(EmailId,data[0]);

        System.out.println(data[0]);

       }

       @Test
        @Given("^Check response body$")
        public void checkResponseBody()
       {
           System.out.println("--------Checking Response body--------");
           Assert.assertTrue(response.body() != null);
       }

       @Test
        @Given("^Check response time$")
        public void checkResponseTime()
       {
           long responseTime = response.getTime();
           if(responseTime>2000)
               System.out.println("response time is very long");
           Assert.assertTrue(responseTime<2000);
       }

       @Test
    @Given("^Check content type$")
    public void checkContentType()
       {
           String contentType = response.header("Content-Type");
           Assert.assertEquals(contentType, "application/json; charset=utf-8");
           System.out.println("-----------content type is : ---------" +contentType);
       }

       @Test
       @Given("^Check server type$")
        public void checkServerType()
       {
           String serverType = response.header("Server");
           System.out.println("Server type is -----" +serverType);
           Assert.assertEquals(serverType , "Kestrel");
       }



}

