Feature:
  Verify the post operation for available endpoints

    Scenario: POST admin users with authentication
    Given I Authenticate the url
    When I perform Post action "http://35.230.156.145/api/v1/Users/addAdminUser" for the emailId "AdminTriad@triad.com"
    Then I should see the status code "200"