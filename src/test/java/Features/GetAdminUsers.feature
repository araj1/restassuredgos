Feature: To read all the admin users

  Scenario: GET admin users with authentication
    Given I Authenticate the url
    When I perform GET operation for "http://35.230.156.145/api/v1/Users/adminusers"
    Then I should see the status code "200"



  Scenario: GET admin users without authentication
    Given I perform GET operation without Authentication for "http://35.230.156.145/api/v1/Users/adminusers"
    Then I should see the status code "401"




