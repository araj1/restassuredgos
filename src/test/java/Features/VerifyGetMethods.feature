Feature:
  Verify different GET operations using REST-assured

  Scenario: Verify the GET request
    Given I Authenticate the url
     When I perform GET operation for "http://35.230.156.145/api/v1/Users/organisation/15"
     Then Check response body
     Then Check response time
     Then Check content type
     Then Check server type
     Then I should see the status code "200"

  Scenario: Verify the GET request and its status
    Given I Authenticate the url
     When I perform GET operation for "http://35.230.156.145/api/v1/users/15/roles/"
     Then I should see the status code "200"



